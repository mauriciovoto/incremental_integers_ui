Thinkific Challenge
===================

This is a simple Single Page Application using AngularJS framework

## Installation

Must have [node and npm](https://nodejs.org/en/download/package-manager/)
installed, since npm is the dependency manager. After installing it, to
installjust the dependencies, you have to run:

```
npm install
```

## Running the Application

To run it locally, the recommended option is to install the angular
[http server](https://www.npmjs.com/package/angular-http-server)

``` 
angular-http-server
```

## Assumptions / Decisions

This section ilustrates some of assumptions and decisions took for the
development process.
As you might know, @mvoto is a backend developer in process of turning into
a fullstack developer. I enjoy learning front-end and I am currently improving
that skills.
The choosen framework was AngularJS cause it is the one that I am most
used to it and it seems to be a good fit for a simple Single Page Application.
Bootstrap framework is here to help building a simple and responsible UI.

## What's next / TODO List

- Add some security to handle the session, ie: use [JWT](https://jwt.io/)
- Add OAuth by using [auth0](https://github.com/auth0/auth0-angular) lib
- Improve UX by adding navbar(with logout, for example) and more info to the user
- Handle specific exceptions, ie.: API not reachable
- Add tests :|