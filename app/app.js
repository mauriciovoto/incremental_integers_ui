var app = angular.module('incrementalIntegersApp', ['ngRoute', 'ngCookies']);

app.config(function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    }).hashPrefix('');
    
    $routeProvider
    .when('/', {
        templateUrl : 'app/views/signup.html',
        controller  : 'signupController'
    })
    .when('/numbers', {
        templateUrl : 'app/views/number.html',
        controller  : 'numberController'
    })    
    .when('/404', {
        templateUrl: 'error.html',
        controller: 'errorController'
    })
    .otherwise({redirectTo:'/404'});    
});

app.constant('config', {  
  apiUrl: 'https://incremental-integers.herokuapp.com/api/v1',
  enableDebug: true
});