app.controller("numberController", function($http, $cookies, $location, $rootScope, config){
  var currentNumber = this;
  currentNumber.apiKey = $cookies.get("token");

  if(!!this.apiKey == false){
    $rootScope.message = "You're not authorized, please signup before"
    $location.path('/');
  }

  function getRequest(url) {
    $http.get(url, {headers: {"Authorization": currentNumber.apiKey}})
    .then(function onSuccess(response){      
      currentNumber.number = response.data.number;
    }).catch(function onError(response){
      message = "An error occurred when fetching our beloved number: "
      currentNumber.number = message + response.data.message;
    });
  };

  getRequest(config.apiUrl + "/current");

  this.update = function() {
    getRequest(config.apiUrl + "/next");
  };

  this.reset = function(number) {
    $http.put(config.apiUrl + "/current", {},
      {headers: {"Authorization": currentNumber.apiKey},
      params: {current: this.resetNumber}}
    ).then(function onSuccess(response){
      currentNumber.number = response.data.number;
    }).catch(function onError(response){
      message = "An error occurred when fetching our beloved number: "
      currentNumber.number = message + response.data.message;
    });
    this.resetNumber = "";
  };
});
