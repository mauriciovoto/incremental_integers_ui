app.controller("signupController", function($location, AuthService, $rootScope, $cookies) {
  $cookies.remove("token");
  this.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/

  var credentials = {
    email: "",
    password: ""
  }

  this.signup = function(credentials) {
    AuthService.login(credentials).then(function(apiKey) {
      if(apiKey === null){
        $rootScope.message = "An error occurred on signup, please try again";
      } else {
        $rootScope.authorized = !!apiKey;
        $cookies.put("token", apiKey);        
        $location.path('/numbers');        
      }
    }).catch(function onError(res){
      console.log(res);
      $rootScope.message = message;
    });
  };
});